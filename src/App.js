import React, {Component} from 'react';
import './App.css';
import youtube from './youtube';
import SearchBar from './Searchbar';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';
// import axios from 'axios';
const KEY = 'AIzaSyDQ9PdNE_3yIvd6sHRSYQ2i0XBty4nTbIo';

class App extends Component{
  state = {
    videos: [],
    selectedVideo: null
  }

  handleSubmit = async (termFromSearchBar) => {
    const response = await youtube.get('/search', {
      params: {
            part: 'snippet',
            maxResults: 5,
            key: KEY,  
            q: termFromSearchBar
      }
    })

    this.setState({
      videos: response.data.items
    })
  };

  handleVideoSelect = (video) => {
    this.setState({selectedVideo: video})
  }


  render() {
    return (
        <div className='ui container' style={{marginTop: '1em'}}>
            <SearchBar handleFormSubmit={this.handleSubmit}/>
            <div className='ui grid'>
                <div className="ui row">
                    <div className="eleven wide column">
                        <VideoDetail video={this.state.selectedVideo}/>
                    </div>
                    <div className="five wide column">
                        <VideoList handleVideoSelect={this.handleVideoSelect} videos={this.state.videos}/>
                    </div>
                </div>
            </div>
        </div>
    )
}
}


// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;
