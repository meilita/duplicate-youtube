import axios from 'axios';
const KEY = 'AIzaSyDQ9PdNE_3yIvd6sHRSYQ2i0XBty4nTbIo';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3/',
    params: {
        part: 'snippet',
        maxResult: 5,
        key: KEY
    }
})

